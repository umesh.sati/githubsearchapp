package com.usati.githubsearchapp.data.repository

import com.usati.githubsearchapp.data.api.RetrofitInstance
import com.usati.githubsearchapp.data.api.SafeApiRequest
import com.usati.githubsearchapp.data.db.RepoDatabase
import com.usati.githubsearchapp.data.models.Item
import com.usati.githubsearchapp.data.models.SearchResponse
import retrofit2.Response

class Repository(
    private val db: RepoDatabase,
    private val retrofit: RetrofitInstance
) : SafeApiRequest() {

    suspend fun searchRepo(searchQuery: String, pageNumber: Int): Response<SearchResponse> {
        return safeApiCall { retrofit.api.getRepositories(searchQuery, pageNumber) }
    }

    suspend fun upsert(repo: Item) = db.getDao().upsert(repo)

    fun getSavedRepo() = db.getDao().getRepos()

    suspend fun delete(repo: Item) = db.getDao().delete(repo)

}