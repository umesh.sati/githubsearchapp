package com.usati.githubsearchapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao
import com.usati.githubsearchapp.data.models.Item

@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(item: Item): Long

    @Query("SELECT * FROM repos")
    fun getRepos(): LiveData<List<Item>>

    @Delete
    suspend fun delete(item: Item)
}