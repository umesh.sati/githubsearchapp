package com.usati.githubsearchapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.usati.githubsearchapp.data.models.Item


@Database(
    entities = [Item::class],
    version = 1
)

@TypeConverters(Converter::class)
abstract class RepoDatabase : RoomDatabase() {

    abstract fun getDao(): RepoDao

    companion object {
        @Volatile
        private var instance: RepoDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                RepoDatabase::class.java,
                "repoDB.db"
            ).build()
    }

}