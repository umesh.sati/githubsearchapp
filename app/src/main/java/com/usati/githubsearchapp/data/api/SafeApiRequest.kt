package com.usati.githubsearchapp.data.api

import okio.IOException
import retrofit2.Response

abstract class SafeApiRequest {
    inline fun<T: Any> safeApiCall(apiCall: () -> Response<T>): Response<T>{
        return try {
            apiCall.invoke()
        } catch (e: Exception){
            throw IOException(e.message)
        }
    }
}