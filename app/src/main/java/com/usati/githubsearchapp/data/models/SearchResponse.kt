package com.usati.githubsearchapp.data.models

data class SearchResponse(
    val incomplete_results: Boolean,
    val items: MutableList<Item>,
    val total_count: Int
)