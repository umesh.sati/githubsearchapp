package com.usati.githubsearchapp.data.api

import com.usati.githubsearchapp.data.models.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubSearchApi {
    @GET("search/repositories")
    suspend fun getRepositories(
        @Query("q")
        searchQuery: String,
        @Query("page")
        pageNumber: Int = 1,
    ): Response<SearchResponse>
}