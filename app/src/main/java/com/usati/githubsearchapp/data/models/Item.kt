package com.usati.githubsearchapp.data.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(
    tableName = "repos"
)
data class Item(
    @PrimaryKey(autoGenerate = false) val id: Long,
    val name: String,
    val full_name: String,
    val description: String?,
    val html_url: String,
    val url: String,
    val stargazers_count: Int,
    val subscribers_count: Int?,
    val forks_count: Int?,
    val language: String?,
    val homepage: String?,
    val created_at: String?,
    @Embedded(prefix = "owner_") val owner: Owner?
) : Serializable