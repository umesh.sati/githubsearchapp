package com.usati.githubsearchapp.utils

class Constants {
    companion object {
        const val BASE_URL = "https://api.github.com"
        const val SEARCH_TIME_DELAY = 50L
        const val QUERY_PAGE_SIZE = 30
    }
}