package com.usati.githubsearchapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.usati.githubsearchapp.data.api.RetrofitInstance
import com.usati.githubsearchapp.data.db.RepoDatabase
import com.usati.githubsearchapp.data.repository.Repository
import com.usati.githubsearchapp.ui.viewmodel.RepoViewModel
import com.usati.githubsearchapp.ui.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: RepoViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val repository = Repository(RepoDatabase(this), RetrofitInstance())
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        viewModel = ViewModelProvider(this, viewModelProviderFactory)[RepoViewModel::class.java]

        bottomNavigationView.background = null
        bottomNavigationView.setupWithNavController(navHostFragment.findNavController())
    }
}