package com.usati.githubsearchapp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.usati.githubsearchapp.data.repository.Repository

class ViewModelProviderFactory(
    private val repository: Repository
) : ViewModelProvider.Factory{

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return RepoViewModel(repository) as T
    }
}