package com.usati.githubsearchapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.usati.githubsearchapp.data.models.Item
import com.usati.githubsearchapp.data.models.SearchResponse
import com.usati.githubsearchapp.data.repository.Repository
import com.usati.githubsearchapp.utils.Resource
import kotlinx.coroutines.launch
import okio.IOException
import retrofit2.Response

class RepoViewModel(
    private val repository: Repository
) : ViewModel() {
    val searchResult: MutableLiveData<Resource<SearchResponse>> = MutableLiveData()
    var pageNo = 1
    var searchResponse: SearchResponse? = null

    fun searchRepos(searchQuery: String) = viewModelScope.launch {
        searchResult.postValue(Resource.Loading())
        try {
            val response = repository.searchRepo(searchQuery, pageNo)
            searchResult.postValue(handleSearchResponse(response))
        }catch (e: IOException){
            searchResult.postValue(Resource.Error(e.message!!))
        }

    }

    fun saveRepo(item: Item) = viewModelScope.launch {
        repository.upsert(item)
    }

    fun deleteRepo(item: Item) = viewModelScope.launch {
        repository.delete(item)
    }

    fun getSavedRepos() = repository.getSavedRepo()

    private fun handleSearchResponse(response: Response<SearchResponse>): Resource<SearchResponse> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                pageNo++
                if (searchResponse == null) {
                    searchResponse = resultResponse
                } else {
                    searchResponse?.items?.addAll(resultResponse.items)
                }
                return Resource.Success(searchResponse!!)
            }
        }
        return Resource.Error(response.message())
    }
}